//
//  Note.swift
//  chum-thea-hw004
//
//  Created by MacBook on 12/9/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

class Note {
    var title: String
    var note: String
    
    init(title: String, note: String) {
        self.title = title
        self.note = note
    }
}
