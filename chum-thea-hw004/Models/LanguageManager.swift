//
//  LanguageManager.swift
//  chum-thea-hw004
//
//  Created by MacBook on 12/11/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation

class LanguageManager {
    static let shared = LanguageManager()
    
    var language: String {
        return UserDefaults.standard.string(forKey: "lang") ?? "en"
    }
    
    func switchLanguage(langugae: Language){
        UserDefaults.standard.set(langugae.rawValue, forKey: "lang")
    }
}

enum Language: String {
    case khmer = "km"
    case english = "en"
}

extension String {
    func localizedString()->String {
        let path = Bundle.main.path(forResource: LanguageManager.shared.language, ofType: "lproj")
          let bundle = Bundle(path: path!)
        return  NSLocalizedString(self, tableName: nil, bundle: bundle!, value: self, comment: self)
    }
}
