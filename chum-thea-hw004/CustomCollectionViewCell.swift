//
//  CustomCollectionViewCell.swift
//  chum-thea-hw004
//
//  Created by MacBook on 12/9/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class CustomCollectionViewCell: UICollectionViewCell, UIGestureRecognizerDelegate {

    @IBOutlet weak var noteTextView: UITextView!
    @IBOutlet weak var seperatorLineView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        noteTextView.showsVerticalScrollIndicator = false
        
        seperatorLineView.layer.borderWidth = 5
        seperatorLineView.layer.borderColor = UIColor.orange.cgColor
        
        customizeCell()
        customizeTitle()
    }
    
    func customizeCell(){
        self.layer.cornerRadius = 8
        self.layer.masksToBounds = true
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.orange.cgColor
    }
    
    func customizeTitle(){
        titleLabel.font = UIFont.preferredFont(forTextStyle: .title1)
    }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        true
    }
}
