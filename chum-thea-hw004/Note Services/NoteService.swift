//
//  NoteService.swift
//  chum-thea-hw004
//
//  Created by MacBook on 12/9/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class  NoteService {
    
    func addNewNote(note: Note){
        guard let applicationDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return
            }
        
            let managedContext = applicationDelegate.persistentContainer.viewContext
            let noteEntity = NSEntityDescription.entity(forEntityName: "Notes", in: managedContext)
            
            let noteManagedObj = NSManagedObject(entity: noteEntity!, insertInto: managedContext)
        
        print(note.title, note.note)
        
        noteManagedObj.setValue(note.title, forKey: "title")
        noteManagedObj.setValue(note.note, forKey: "note")
        do{
            try managedContext.save()
        }catch let error{
            print(error)
        }

    }
    
    
    func fetchAllNotes() -> [Note]?{
        var noteArray: [Note] = []
        guard let applicationDelegate = UIApplication.shared.delegate as? AppDelegate else {
                return nil
        }
        
        let managedContext = applicationDelegate.persistentContainer.viewContext
        let fetchResult = NSFetchRequest<NSManagedObject>(entityName: "Notes")
        
        do{
            let notes = try managedContext.fetch(fetchResult)
            for note in notes {
                let title = note.value(forKey: "title") as! String
                let note = note.value(forKey: "note") as! String
                
                let noteObj = Note(title: title, note: note)
                noteArray.append(noteObj)
            }
        }catch let error {
            print(error)
        }
        
        return noteArray
        
    }
    
    func deleteNote(titleToDelete: String){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        fetchRequest.predicate = NSPredicate(format: "title = %@", "\(titleToDelete)")
        
        do{
            let test = try managedContext.fetch(fetchRequest)
            
            let noteToDelete = test[0] as! NSManagedObject
            managedContext.delete(noteToDelete)
            
            do{
                try managedContext.save()
            }catch {
                print(error)
            }
        }catch {
            print(error)
        }
    }
    
    func updateNote(note: Note, byTitle title: String){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {return}
        
        let managedContext = appDelegate.persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Notes")
        fetchRequest.predicate = NSPredicate(format: "title = %@", "\(title)")
        
        do{
            let test = try managedContext.fetch(fetchRequest)
            
            let noteToUpdate = test[0] as! NSManagedObject
            noteToUpdate.setValue(note.title, forKey: "title")
            noteToUpdate.setValue(note.note, forKey: "note")

            do{
                try managedContext.save()
            }catch {
                print(error)
            }
        }catch {
            print(error)
        }
    }
}
