//
//  NewNoteViewController.swift
//  chum-thea-hw004
//
//  Created by MacBook on 12/9/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit
import CoreData

class NewNoteViewController: UIViewController {

    var noteService: NoteService?
    
    @IBOutlet weak var separatorLineView: UIView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var noteTextField: UITextField!
    
    @IBOutlet weak var saveBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var pointBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var pinBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var bottomRightBartButtonItem: UIBarButtonItem!
    @IBOutlet weak var bottomLelfBarButtonItem: UIBarButtonItem!
    
    
    
    var newOrUpdateText = "Add New Note"
    var titleText = ""
    var noteText = ""
    var isAdd = true
    var titleTmpString: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpLanguage()
        
//        newOrUpdateLabel.text = newOrUpdateText
        titleTextField.text = titleText
        noteTextField.text = noteText

        titleTmpString = titleTextField.text
        noteService = NoteService()
        customizeSeperatorLineView()
        
        customizeTextFieldBorder(textField: titleTextField, placeHolderString: "title".localizedString())
        customizeTextFieldBorder(textField: noteTextField, placeHolderString: "description".localizedString())
        
        customizeBarButtonItem(saveBarButtonItem)
        customizeBarButtonItem(pointBarButtonItem)
        customizeBarButtonItem(pinBarButtonItem)
        customizeBarButtonItem(bottomLelfBarButtonItem)
        customizeBarButtonItem(bottomRightBartButtonItem)
        
       customNavBarTitle()
    }
    
    func setUpLanguage(){
        if isAdd {
            navigationItem.title = "addNewNote".localizedString()
        }else {
            navigationItem.title = "editNote".localizedString()
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            let titleString = titleTextField.text!
            let noteString = noteTextField.text!
            let note = Note(title: titleString, note: noteString)

            if isAdd {
                
                noteService?.addNewNote(note: note)
                navigationController?.popViewController(animated: true)
                
            }else {

                noteService?.updateNote(note: note, byTitle: titleTmpString!)
                navigationController?.popViewController(animated: true)
                
            }
        }
    }

    func customNavBarTitle(){
        self.navigationController?.navigationBar.tintColor = .orange
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.orange] as [NSAttributedString.Key: Any]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.black] as [NSAttributedString.Key: Any]
    
    }

    func customizeBarButtonItem(_ barButtonItem: UIBarButtonItem){
        barButtonItem.tintColor = UIColor.orange
    }
    
    func customizeSeperatorLineView(){
        separatorLineView.layer.borderWidth = 5
        separatorLineView.layer.borderColor = UIColor.orange.cgColor
    }
    
    func customizeTextFieldBorder(textField: UITextField, placeHolderString: String){
        textField.borderStyle = .none
        textField.layer.masksToBounds = true
        textField.layer.borderWidth = 0
        textField.layer.borderColor = UIColor.clear.cgColor
        textField.placeholder = placeHolderString
    }
    
    
    @IBAction func rightBarButtonItemPressed(_ sender: UIBarButtonItem) {
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let deleteAction = UIAlertAction(title: "delete".localizedString(), style: .default, handler: nil)
        let copyAction = UIAlertAction(title: "copy".localizedString(), style: .default, handler: { (action) in
            print(action)
        })
        let sendAction = UIAlertAction(title: "send".localizedString(), style: .default, handler: nil)
        let colabratorAction = UIAlertAction(title: "colabrator".localizedString(), style: .default, handler: { (action) in
                  print(action)
        })
        let labelAction = UIAlertAction(title: "label".localizedString(), style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "cancel".localizedString(), style: .cancel, handler: { (action) in
            print(action)
        })
        
        alert.addAction(deleteAction)
        alert.addAction(copyAction)
        alert.addAction(sendAction)
        alert.addAction(colabratorAction)
        alert.addAction(labelAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true) {
            print("right")
        }
    }
    
    
    @IBAction func letBarButtonItemPressed(_ sender: UIBarButtonItem){
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let takePhotoAction = UIAlertAction(title: "takeAPhoto".localizedString(), style: .default, handler: nil)
        let chooseImageAction = UIAlertAction(title: "chooseImage".localizedString(), style: .default, handler: { (action) in
            print(action)
        })
        let drawingAction = UIAlertAction(title: "Drawing".localizedString(), style: .default, handler: nil)
        let recordingAction = UIAlertAction(title: "Recording".localizedString(), style: .default, handler: { (action) in
                  print(action)
        })
        let checkBoxsAction = UIAlertAction(title: "checkboxs".localizedString(), style: .default, handler: nil)
        let cancelAction = UIAlertAction(title: "cancel".localizedString(), style: .cancel, handler: { (action) in
            print(action)
        })
        
        alert.addAction(takePhotoAction)
        alert.addAction(chooseImageAction)
        alert.addAction(drawingAction)
        alert.addAction(recordingAction)
        alert.addAction(checkBoxsAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true) {
            print("More")
        }
    }
    
}
