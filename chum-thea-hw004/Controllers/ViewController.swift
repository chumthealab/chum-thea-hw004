//
//  ViewController.swift
//  chum-thea-hw004
//
//  Created by MacBook on 12/9/19.
//  Copyright © 2019 MacBook. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var noteCollectView: UICollectionView!
    @IBOutlet weak var takeNoteHereButton: UIBarButtonItem!
    
    
    
    @IBOutlet weak var translateBarBtnItem: UIBarButtonItem!
    @IBOutlet weak var searchBarBtnItem: UIBarButtonItem!
    @IBOutlet weak var textNewNoteBarBtnItem: UIBarButtonItem!
    @IBOutlet weak var cateBarBtnItem: UIBarButtonItem!
    @IBOutlet weak var recordBarBtnItem: UIBarButtonItem!
    @IBOutlet weak var penBarBtnItem: UIBarButtonItem!
    @IBOutlet weak var cameraBarBtnItem: UIBarButtonItem!
    
    
    var noteService: NoteService?
    var notes : [Note]?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpLanguage()
        
        customizeBarButtonItem(translateBarBtnItem)
        customizeBarButtonItem(searchBarBtnItem)
        customizeBarButtonItem(textNewNoteBarBtnItem)
        customizeBarButtonItem(cateBarBtnItem)
        customizeBarButtonItem(recordBarBtnItem)
        customizeBarButtonItem(penBarBtnItem)
        customizeBarButtonItem(cameraBarBtnItem)
        
        
        
        notes = []
        noteService = NoteService()
        notes = noteService?.fetchAllNotes()
        noteCollectView.dataSource = self
        noteCollectView.delegate = self
        noteCollectView.register(UINib(nibName: "CustomCollectionViewCell", bundle: nil),
                                 forCellWithReuseIdentifier: "CustomCollectionViewCellIdentifier")
        
        let tapGesture = UITapGestureRecognizer(target: self,
                                                action: #selector(gesture(tapGesture:)))
        tapGesture.numberOfTapsRequired = 1
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.delegate = self
        noteCollectView.addGestureRecognizer(tapGesture)
        
        let tapHoldPressed = UILongPressGestureRecognizer(target: self,
                                                          action: #selector(longPressedGesture(gesture:)))
        tapHoldPressed.delegate = self
        noteCollectView.addGestureRecognizer(tapHoldPressed)
        
        noteCollectView.showsVerticalScrollIndicator = false
  
    }
    
    func setUpLanguage(){
        navigationItem.title = "note".localizedString()
        takeNoteHereButton.title = "takeANote".localizedString()
    }
    
    @IBAction func googleTranslatePressed(_ sender: UIButton) {
        
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        alert.addAction(UIAlertAction(title: "khmer".localizedString(),
                                      style: .default,
                                      handler: { (action) in
            LanguageManager.shared.switchLanguage(langugae: .khmer)
            self.setUpLanguage()
        }))
        
        alert.addAction(UIAlertAction(title: "english".localizedString(),
                                      style: .default,
                                      handler: { (action) in
            LanguageManager.shared.switchLanguage(langugae: .english)
            self.setUpLanguage()
        }))
        
        alert.addAction(UIAlertAction(title: "cancel".localizedString(),
                                      style: .cancel,
                                      handler: { (action) in
        }))
        
        present(alert, animated: true) {
            print("alert language")
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        notes = noteService?.fetchAllNotes()
        noteCollectView.reloadData()
    }
    
    // one click to update
    @objc func gesture(tapGesture: UIGestureRecognizer){
        let point = tapGesture.location(in: noteCollectView)
        if let indexPath = noteCollectView?.indexPathForItem(at: point) {
            let cell = self.noteCollectView.cellForItem(at: indexPath) as! CustomCollectionViewCell
            
            let title = cell.titleLabel.text!
            let description = cell.noteTextView.text!
            
            let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
            let addViewController = storyBoard.instantiateViewController(identifier: "NewNoteViewControllerIdentifier") as! NewNoteViewController

            addViewController.titleText = title
            addViewController.noteText = description
            addViewController.isAdd = false
            
            navigationController?.pushViewController(addViewController, animated: true)
        }
    }
    
    // method to handle with long pressed
    @objc func longPressedGesture(gesture: UILongPressGestureRecognizer){
        if gesture.state != .ended {
            return
        }
        
        let point = gesture.location(in: noteCollectView)
        
        if let indexPath = self.noteCollectView.indexPathForItem(at: point){
            
            let cell = self.noteCollectView.cellForItem(at: indexPath) as! CustomCollectionViewCell
            
            let alert = UIAlertController(title: nil,
                                          message: "deleteComfirmation".localizedString(),
                                          preferredStyle: .actionSheet)
            
            alert.addAction(UIAlertAction(title: "yes".localizedString(),
                                          style: .destructive,
                                          handler: { (action) in
                self.noteService?.deleteNote(titleToDelete: cell.titleLabel.text!)
                self.noteCollectView.reloadData()
                self.notes?.remove(at: indexPath.row)
            }))
            
            alert.addAction(UIAlertAction(title: "cancel".localizedString(), style: .cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
        }
    }
 
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          notes!.count
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
         
        let noteArray = notes!
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCellIdentifier", for: indexPath) as! CustomCollectionViewCell
         cell.titleLabel.text = noteArray[indexPath.row].title
         cell.noteTextView.text = noteArray[indexPath.row].note
        
         return cell
      }
    

}

extension ViewController : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.view.frame.width - 50) / 2, height: self.view.frame.height / 4)
    }
}

extension ViewController : UIGestureRecognizerDelegate{
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        let point = touch.location(in: noteCollectView)
        if let indexPath = noteCollectView.indexPathForItem(at: point),
            let cell = noteCollectView.cellForItem(at: indexPath){
            return touch.location(in: cell).y > 50
        }
        return false
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
    
    func customizeBarButtonItem(_ barButtonItem: UIBarButtonItem){
        barButtonItem.tintColor = UIColor.orange
    }
}


